from django.apps import AppConfig


class S01ActivityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'S01_Activity'
